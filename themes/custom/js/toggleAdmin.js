(function ($) {
  Drupal.behaviors.toggleAdmin = {
    attach: function(context) {

      var devmode = true; // set to true to enable the toggle button

      if ($('body').hasClass('logged-in') && devmode) $('body', context).once('toggleAdmin', function() {

        var hideThese = [
          '.tabs',
          '#admin-menu',
          '#navbar-administration',
          'ul.links.inline',
          'ul.links.inline:not(.main-menu)',
          'ul.links.inline:not(.secondary-menu)',
          '.action-links',
          '.admin.button',
          '.block-masquerade',
          '.slicknav_menu'
        ];

        var toggleButton = $('<button class="button admin-extra js_toggle-admin">Toggle Admin</button>');

        toggleButton.css({ "position": "fixed", "bottom": "0", "right": "0", "z-index": "9999"}).bind('click',toggle);

        $('body').append(toggleButton);

        function toggle() {
          if ($('body.admin-menu').length > 0 || $('body.navbar-administration').length > 0) {
            $('body').toggleClass('js_no-admin-margin');
          }

          for(var i = 0, len = hideThese.length; i < len; i++) {
            if ($(hideThese[i]).length) $(hideThese[i]).toggleClass('js_hide');
          }
          $(this).toggleClass('js_admin-hidden');

          if ($(this).hasClass('js_admin-hidden')) {
            $(this).html('Show Admin');
          } else {
            $(this).html('Hide Admin');
          }
        }

      });
    }
  };
})(jQuery);