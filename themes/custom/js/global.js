(function ($, window, undefined) {

  var themeMode = function() {
    // see global.less for .theme_mode fake element
    var themeElClass = 'theme_mode';
    var $themeEl = $('<div>&nbsp;</div>');
    $themeEl.addClass(themeElClass);

    $('body').append($themeEl);

    // Get pseudo element content, strip quotation marks, and make it a number
    // See: https://davidwalsh.name/pseudo-element
    var pseudoValue = window.getComputedStyle(document.querySelector('.'+themeElClass), ':before').getPropertyValue('content').replace(/["']/g, '');

    $themeEl.remove();

    return pseudoValue;
  };


  var breaks = [
    'mobilemax',
    'babybear',
    'tabletmax',
    'goldilocks',
    'mamabear',
    'papabear'
  ];

  // Get breaks from less
  var recordWidths = function(point) {
    var $fakeEl = $('<div/>');
    var fakeElClass = 'js-breaks__'+point;

    $fakeEl.addClass(fakeElClass);

    var bp = {};
    var currBreaks = !!$('body').attr('data-breaks') ? JSON.parse($('body').attr('data-breaks')) : {};

    $('body').prepend($fakeEl);

    // Get pseudo element content, strip quotation marks, and make it a number
    // See: https://davidwalsh.name/pseudo-element
    var pseudoValue = parseInt(window.getComputedStyle(document.querySelector('.'+fakeElClass), ':before').getPropertyValue('content').replace(/["']/g, ''), 10);

    currBreaks[point] = pseudoValue;

    $('body').attr('data-breaks', JSON.stringify(currBreaks));

    $fakeEl.remove();
  };


  var bp = function(name) {
    return JSON.parse($('body').attr('data-breaks'))[name];
  };

  var convertUnit = function(px) {
    var value = String(parseFloat(px)),
        unit = String(px).replace(value, '');

    switch (unit) {
      case 'em':
        return value / parseFloat($('body').css('font-size')) + 'em';
      break;
      case 'rem':
        return value / parseFloat($('body').css('font-size')) + 'rem';
      break;
      default:
        // px
        return value + 'px';
      break;
    }
  };

  var jsMediaQuery = function(mq, cb) {
    var mediaQuery = window.matchMedia(mq);

    function doSomething(mediaQuery) {
      if (mediaQuery.matches) {
        return cb();
      }
    }
    // Set listener
    mediaQuery.addListener(doSomething);

    // Call for initial check
    doSomething(mq);
  };

  var resp = function(size, cb) {
    var mq = '(max-width: '+ convertUnit(size) +')';
    jsMediaQuery(mq, cb);
  };

  var respMin = function(size, cb) {
    var mq = '(min-width: '+ convertUnit(size) +')';
    jsMediaQuery(mq, cb);
  };

  var respRange = function(min, max, cb) {
    var mq = '(min-width: '+ convertUnit(min) +') and (max-width: '+ convertUnit(max) +')';
    jsMediaQuery(mq, cb);
  };


  Drupal.behaviors.MQS = {
    attach: function(context) {
      breaks.map(recordWidths);
    }
  };

  $.fn.attachConfirmEmail = function() {
    // console.log($(this).data('confirm-source'));
    if (!$(this).data('confirm-source')) {
      return;
    }
    var src = $(this).data('confirm-source');
    var email = $('input[name^="' + src + '"]');
    if (!$(email).length) {
      return;
    }

    $(this).after('<div id="field-confirm-mail-msg"></div>');

    var confirmEmail = this;

    $(email).bind('blur', function() {
      checkConfirmEmail(email, confirmEmail);
    });
    $(this).bind('keyup', function() {
      checkConfirmEmail(email, confirmEmail);
    });
    function checkConfirmEmail(email, confirmEmail) {
      if (!$(email).val() || !$(confirmEmail).val()) {
        // Don't do anything if one of these is empty.
        return;
      }
      if ($(email).val() != $(confirmEmail).val()) {
        if ($(confirmEmail).hasClass('js-error')) {
          // Already flagged for error
          return;
        }
        // Using js error so that it still shows the message even if there's a drupal error class already added
        $(confirmEmail)
          .addClass('js-error')
          .addClass('error');
        $('#field-confirm-mail-msg')
          .text(Drupal.t('Email addresses do not match'));
      }
      else {
        $(confirmEmail)
          .removeClass('js-error')
          .removeClass('error');
        $('#field-confirm-mail-msg').empty();
      }
    }
  }

  Drupal.behaviors.crvpTmpDisable = {
    attach: function(context, settings) {
      if (!!Drupal.settings.cvrp_income_launched) {
        // Abort now if the kill switch is set.
        return;
      }
      if (!Drupal.settings.cvrp_income_block_paths) {
        // Can't do anything without a block list.
        return;
      }
      var base = '';
      if (!Drupal.settings.base_url) {
        base = window.location.protocol + '//' + window.location.hostname;
      }
      else {
        base = Drupal.settings.base_url;
      }

      $(Drupal.settings.cvrp_income_block_paths).each(function() {
        console.log(this);
        var selector = '';
        var path = '/' + Drupal.settings.pathPrefix + this;
        if (this.indexOf('*') > -1) {
          // starts with
          path = path.slice(0, -1);
          selector = 'a[href^="' + path + '"], a[href^="' + base + path + '"], a[href^="/' + this + '"], a[href^="' + base + '/' + this + '"]';
        }
        else {
          // equivalent
          selector = 'a[href="' + path + '"], a[href="' + base + path + '"], a[href="/' + this + '"], a[href="' + base + '/' + this + '"]';
        }
        console.log(selector);
        $(selector)
          .addClass('disabled')
          .attr('href', 'javascript:;')
          .attr('title', Drupal.t('Performing scheduled maintenance. Temporarily unavailable.'));
      });
    }
  };

  Drupal.behaviors.confirmEmail = {
    attach: function(context, settings) {
      if (!$('.confirm-email').length) {
        return;
      }
      $('.confirm-email').each(function() {
        $(this).attachConfirmEmail();
      });
    }
  }

  Drupal.behaviors.viewsFilterRefocus = {
    attach: function(context, settings) {
      function SetCaretAtEnd(elem) {
        var elemLen = elem.value.length;
        // For IE Only
        if (document.selection) {
            // Set focus
            elem.focus();
            // Use IE Ranges
            var oSel = document.selection.createRange();
            // Reset position to 0 & then set at end
            oSel.moveStart('character', -elemLen);
            oSel.moveStart('character', elemLen);
            oSel.moveEnd('character', 0);
            oSel.select();
        }
        else if (elem.selectionStart || elem.selectionStart == '0') {
            // Firefox/Chrome
            elem.selectionStart = elemLen;
            elem.selectionEnd = elemLen;
            elem.focus();
        } // if
      } // SetCaretAtEnd()

      var textboxToFocus = {};

      var addFocusReminder = function(textbox) {
        textbox.bind('keypress keyup', function(e) {
          textboxToFocus.formid = $(this).closest('form').attr('id');
          textboxToFocus.name = $(this).attr('name');

          if(e.type == 'keypress') {
            if(e.keyCode != 8) { // everything except return
              textboxToFocus.value = $(this).val() + String.fromCharCode(e.charCode);
            } else {
              textboxToFocus.value = $(this).val().substr(0, $(this).val().length-1)
            }
          }
          else { // keyup
            textboxToFocus.value = $(this).val();
          }
        });
      }

      addFocusReminder($('.view-filters input:text.ctools-auto-submit-processed'));
      $(document).ajaxComplete(function(event,request, settings) {
        + addFocusReminder($('.view-filters input:text.ctools-auto-submit-processed'));
        if(typeof textboxToFocus.formid !== 'undefined') {
            var textBox = $('#' + textboxToFocus.formid + ' input:text[name="' + textboxToFocus.name + '"]');
            textBox.val(textboxToFocus.value);
            SetCaretAtEnd(textBox[0]);
            addFocusReminder(textBox);
            //textboxToFocus = {}; // if you have other auto-submitted inputs as well
        }
      });
    }
  }

  Drupal.behaviors.hideSubmitButton = {
    attach: function(context, settings) {
      if (!$('body.pfp-workflow, form#cvrp-application-node-form').length) {
        return;
      }
      var $waitButton = $('<p id="submit-loading-wait">' + Drupal.t('Waiting for response...') + '</p>');

      $('form').submit(function(e) {
        var $submitButton = $(this).find('.form-actions [id*="edit-submit"]');
        if (!!e._hasSubmitted || $submitButton.hasClass('hasThrobber')) {
          return false;
        }
        else {
          e._hasSubmitted = true;
          $submitButton.addClass('hasThrobber');
          $submitButton.after($waitButton);
          $submitButton.hide();
          return true;
        }
      });

      $(window).keydown(function(event){
        if(event.keyCode == 13) {
          event.preventDefault();
          return false;
        }
      });
    }
  };


  Drupal.behaviors.fileSelect = {
    attach: function(context) {

      // $(document).delegate('.label-mate', 'click', function(event) {
      //   $(this).closest('.file-wrapper').find('label').trigger('click');
      // });

      $(document).delegate(':file', 'change', function(event) {
          var input = $(this),
              label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
          input.trigger('fileselect', [label, Boolean(input.val().length)]);
      });

      $(document).delegate(':file', 'fileselect', function(event, label, hasFile) {
          event.preventDefault();
          var $input = $(this),
              $labelMate = $(this).siblings('.label-mate'),
              $wrapper = $input.closest('.file-wrapper'),
              $uploadButton = $input.closest('.form-wrapper').find('.upload-button');

          $wrapper.toggleClass('file-chosen', hasFile);
          $labelMate.text(hasFile ? label : 'No file selected');
          $uploadButton.toggleClass('file-chosen', hasFile);

          // Append a reset button
          if (hasFile && !$wrapper.find('.reset').length) {
            $labelMate.after(
              $('<button class="reset"><b class="close"><svg class="icon"><use xlink:href="#x" /></svg></b><span class="text">Reset</span></button>').bind('click', function(e) {
                $input.val('').trigger('change');
                $(this).remove();
              })
            );
          }
      });
    }
  };

  Drupal.behaviors.uploadUnsubmitWarning = {
    attach: function(context, settings) {
      window.onbeforeunload = function() {
        if ($('#energycenter-cvrp-documents-form input[type="file"]').filter(function() { return !!$(this).val(); }).length > 0) {
          return "You have unsubmitted documents.";
        }
      }
    }
  };

  Drupal.behaviors.dateAllDay = {
    attach: function(context, settings) {
      // Automatically set the "all day" flag if no time is entered.
      $('form').once('all-day-auto', function() {
        if (!$('div.field-type-datetime').length) {
          // only apply to forms that actually have date fields.
          return;
        }
        $(this).bind('submit', function() {
          // On form submit, if time field is empty for any "all day" date
          // field, check the "all day" box for this field.
          $('div.field-type-datetime input.hasTimeEntry').each(function() {
            var wrapper = $(this).parents('.fieldset-wrapper');
            if (!$(this).val()) {
              if ($('input[id$="all-day"][type="checkbox"]', wrapper).length) {
                $('input[id$="all-day"][type="checkbox"]', wrapper).attr('checked', 'checked');
              }
            }
          });
        });
      });
    }
  };

  Drupal.behaviors.activeClassByPath = {
    attach: function(context, settings) {
      var parts = window.location.pathname.split('/');
      var selectors = '';
      var cur_path = '';
      $(parts).each(function(k, v) {
        // console.log(k);
        // console.log(v);
        if (v == '') {
          return;
        }
        cur_path = cur_path + '/' + v;
        if (selectors != '') {
          selectors = selectors + ', ';
        }
        selectors = selectors + 'a[href^="' + cur_path + '"]';
      });
      $(selectors)
        .addClass('active-trail');
    }
  };

  Drupal.behaviors.animatedAnchorLinks = {
      attach: function(context, settings) {
        // Don't animate these
        var animatedIgnore = [
          '.contextual-links-trigger',
          '.tabs a',
          '.ui-state-default'
        ].join(', ');
        // animate anchor link scrolling:
        $('a[href*="#"]').not($(animatedIgnore)).once('animated', function() {

          function CheckSlugValidity(slug){
            if (window.location.pathname === '/') {
              return false;
            }
            return slug.indexOf(window.location.pathname) === 0;
          }

          $(this).on('click.animated', function(e) {
            var id = $(this).attr('href');
            if (id.length > 1 && id.substring(0,1) !== "#") {
              if (!CheckSlugValidity(id)) {
                return true;
              }
              id = id.substring(id.indexOf('#'));
            }
            e.preventDefault();
            if (!id.length || id == '#' || !$([id, 'a[name=' + id.substring(1) + ']'].join(', ')).length) {
              return false;
            }
            // Figure out how many pixels we're going to be moving.
            // Set scroll speed to 2000 px / second.
            var pos = $(window).scrollTop();
            var dest = ($(id).length) ? $(id).offset().top : $('a[name=' + id.substring(1) + ']').offset().top;
            // pad out to the height of the jump menu.
            var padding = $('#jump-nav').height();
            dest = dest - padding;
            if (dest < 0) {
              dest = 0;
            }
            var speed = Math.abs(pos - dest) / 2;
            $('html,body').stop().animate({
              scrollTop: dest
            }, speed);
            return false;
          });
        });
      }
    };

  Drupal.behaviors.externalLinks = {
    attach: function (context) {
      if (!!Drupal.settings.no_target_blank_external_links) {
        // from php, do:
        //drupal_add_js(array('no_target_blank_external_links' => 1), 'setting')
        // if you don't want external links to open in a new window.
        return;
      }
      var base = '';
      if (!Drupal.settings.base_url) {
        base = 'http://' + window.location.hostname;
      }
      else {
        base = Drupal.settings.base_url;
      }
      var pattern = 'a[href^="https://"]:not(a[href^="' + base.replace(/http/, 'https') + '"]), a[href^="http://"]:not(a[href^="' + base + '"])';
      $(pattern).each(function() {
        $(this).attr('target', '_blank');
      });
    }
  };

  Drupal.behaviors.filesInNewWindow = {
    attach: function(context, settings) {
      $('.field-name-field-files a').attr('target', '_blank');
    }
  };

  Drupal.behaviors.ghostText = {
    attach: function (context) {
      $('input.ghost-text', context).once('ghost-processed', function () {
        $(this).ghost();
      });

      // Prevent submission of ghost text.
      $('form').once('ghost-processed', function() {
        $(this).submit(function() {
          $(this).find('input.ghosted').val('');
        });
      });
    }
  };

  Drupal.behaviors.shareLinks = {
    attach: function(context) {
      if (!$('.share-links').length) return false;

      $('.share-links').once('share-links', function(){
        var $shareLinks = $('.share-links');
        var $shareLinksToggle = $('<span class="share-links__toggle toggle isClosed"></span>');
        // var $printButton = $('<a href="#" class="print-button">Print</a>');

        $shareLinksToggle.bind('click', function(e) {
          e.preventDefault();
          e.stopPropagation();

          $(this).toggleClass('isClosed');
          $shareLinks.toggleClass('isClosed');

          if (!$($shareLinks).hasClass('hasTransition')) {
            $($shareLinks).addClass('hasTransition');
          }
        });

        /*
        $printButton.bind('click', function(e) {
          e.preventDefault();
          e.stopPropagation();
          window.print();
        });
        //*/

        $shareLinks
          .addClass('isClosed')
          .before($shareLinksToggle);
        //$shareLinks.after($printButton);
      });
    }
  };

  Drupal.behaviors.moveAddToCalendar = {
    attach: function(context) {
      $('.addtocal').appendTo('#block-system-main');
      $('.addtocal_menu').appendTo('.addtocal');
    }
  };

  Drupal.behaviors.bannerImgs = {
    attach: function(context) {
      if (!$('.banner[data-banner-images]').length) return false;

      var $banner = $('.banner[data-banner-images]'),
          randImgs = JSON.parse($banner.attr('data-banner-images')),
          i = Math.floor(Math.random() * randImgs.length);

      $banner
        .attr('style', 'background-image: url(' +randImgs[i].img+ ');')
        .removeClass('banner')
        .addClass(randImgs[i].style);
    }
  };

  Drupal.behaviors.owlCarousel = {
    attach: function(context) {
      $('.view-cvrp-eligible-vehicles.view-display-id-block_1 > .view-content > .view-content-inner').once('owlCarousel', function() {
        var $owl = $(this);

        $owl.hide();

        function moveOwlNav() {
          $owl.show();
          $owl.after($owl.find('.owl-controls'));
        }

        var arrowId = (function() {
          var arrowset = {
            left: '#left_arrow',
            right: '#right_arrow'
          };

          switch(themeMode()) {
            case 'classic':
              arrowset.left = '#arrow-left-classic';
              arrowset.right = '#arrow-right-classic';
            break;
          }
          return arrowset;
        })();

        var navText = [
          '<svg class="icon"><use xlink:href="'+arrowId.left+'" /></svg><span class="text">Prev</span>',
          '<svg class="icon"><use xlink:href="'+arrowId.right+'" /></svg><span class="text">Next</span>'
        ];

        var owlOptions = {
          loop: true,
          dots: false,
          navText: navText,
          autoplay: true,
          autoplayHoverPause: true,
          onInitialized: moveOwlNav,
          responsive: {
            0: {
              items: 1,
              nav: false,
              margin: 0,
              responsiveClass: true
            }
          }
        };

        owlOptions.responsive[bp('babybear')] = {
          items: 3,
          nav: true,
          margin: 20
        };
        owlOptions.responsive[bp('tabletmax')] = {
          items: 4,
          nav: true,
          margin: 20
        };
        owlOptions.responsive[bp('goldilocks')] = {
          items: 5,
          nav: true,
          margin: 20
        };

        $owl.owlCarousel(owlOptions);

        $(document.documentElement).keydown(function(event) {
          if (event.keyCode == 37) {
            $owl.data('owlCarousel').prev();
          } else if (event.keyCode == 39) {
            $owl.data('owlCarousel').next();
          }
        });

        $("#next").on('click',function(e) {
          var $focusedElement = $(document.activeElement),
          type = e.which == 39? 'next': null,
          type = e.which == 37? 'prev': type;

          if($focusedElement.data('.owl-carousel')){
            $focusedElement.trigger(type+'.owl.carousel');
          }
        });

      });
    }
  };

  Drupal.behaviors.closeMessage = {
    attach: function(context) {
      $('.messages, .cvrp-announcement').once('closeMessage', function() {
        var $self = $(this);
        $('<button class="close"><svg class="icon icon--close"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#x"></use></svg><span class="text">Close</span></button>')
          .on('click', function(e) {
            e.preventDefault();
            $self.addClass('isClosed');
          }).appendTo($self);
      });
    }
  };

    Drupal.behaviors.toggleTopNav = {
    attach: function(context) {
      $('body', context).once('toggleTopNav', function() {
        var $topLevelNav = $('.top-level-nav', context);
        var toggleText = $topLevelNav.attr('data-toggle-text');

        $topLevelNav.ma_ToggleSwitch({
          'toggleClass': 'mainMenuToggle',
          'bodyClass': 'topNav',
          toggleOnText: toggleText,
          toggleOffText: toggleText,
          'switchElement': 'span',
            'closeOthers': [
             '.tlAccSwitch'
            ]
        }).render();
      });
    }
  };

  Drupal.behaviors.topLevelNavAccorions = {
    attach: function(context) {
      $('body', context).once('topLevelNavAccorions', function() {
        // Top level items accordion
        $('.top-level-nav .menu-block-wrapper > .menu > .expanded, .top-level-nav .content > .menu > .expanded', context).each(function(i,el) {
          $(this).ma_ToggleSwitch({
            'insertTarget': $(this).find('> a, > .nolink'),
            'insertWhere': 'after',
            'switchElement': 'span',
            'getsToggle': $(this).find('> .menu'),
            'paneClass': 'tlAccordion',
            'toggleClass': 'tlAccSwitch',
            'closeOthers': [
              '.tlAccSwitch' // Enable to make true accordion
            ]
          }).render(function() {
            $(el).find('> a, > .nolink, .tlAccSwitch').wrapAll('<div class="tlAccSwitchWrap"></div>');

            $(el).find('.tlAccSwitch').on('click.toggle closeMe', function() {
              $('.tlAccOpen').removeClass('tlAccOpen');
              if ($(this).hasClass('isOpen')) {
                $(this).closest('li').addClass('tlAccOpen');
              }
            });
          });
        });

        // Top level sub items Accordion
        $('.top-level-nav .menu-block-wrapper > .menu .menu > .expanded', context).each(function(i,el) {
          $(this).ma_ToggleSwitch({
            'insertTarget': $(this).find('> a, > .nolink'),
            'insertWhere': 'after',
            'switchElement': 'span',
            'getsToggle': $(this).find('> .menu'),
            'paneClass': 'slAccordion',
            'toggleClass': 'slAccSwitch',
            'closeOthers': [
              '.slAccSwitch' // Enable to make true accordion
            ]
          }).render(function() {
            $(el).find('> a, > .nolink, .slAccSwitch').wrapAll('<div class="slAccSwitchWrap"></div>');

            $(el).find('.slAccSwitch').on('click.toggle closeMe', function() {
              $('.slAccOpen').removeClass('slAccOpen');
              if ($(this).hasClass('isOpen')) {
                $(this).closest('li').addClass('slAccOpen');
              }
            });
          });
        });
      });
    }
  };


})(jQuery, window);
