(function($) {
  Drupal.behaviors.sameHeights = {
    attach: function (context) {
      $('body', context).once('sameheights', function () {
        $(window).bind('resize.sameheights', function () {
          $($('.same-height-container').get().reverse()).each(function () {
            var elements = $('.same-height-element', this).css('height', '');
            
            if (!Drupal.behaviors.hasOwnProperty('omegaMediaQueries') || Drupal.omega.getCurrentLayout() != 'mobile') {
              var tallest = 0;

              elements.each(function () {    
                if ($(this).height() > tallest) {
                  tallest = $(this).height();
                }
              }).each(function() {
                if ($(this).height() < tallest) {
                  $(this).css('height', tallest);
                }
              });
            }
          });
        }).load(function () {
          $(this).trigger('resize.sameheights');
        });
      });
    }
  };
})(jQuery);