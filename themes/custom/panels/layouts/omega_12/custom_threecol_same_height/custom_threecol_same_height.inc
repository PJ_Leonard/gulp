<?php

/**
 * Implements hook_panels_layouts().
 */
function custom_custom_threecol_same_height_panels_layouts() {
  $items['custom_threecol_same_height'] = array(
    'title' => t('Three column same height'),
    'category' => t('Custom'),
    'icon' => 'custom_threecol_same_height.png',
    'theme' => 'custom_threecol_same_height',
    'admin css' => '../omega_panels_admin_12.css',
    'regions' => array(
      'left' => t('Left side'),
      'middle' => t('Middle column'),
      'right' => t('Right side')
    ),
  );

  return $items;
}
