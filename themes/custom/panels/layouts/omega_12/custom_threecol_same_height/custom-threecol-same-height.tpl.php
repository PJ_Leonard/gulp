<div class="same-height-container panel-display omega-grid custom-threecol-same-height" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
  <div class="same-height-element panel-panel grid-4">
    <div class="inside"><?php print $content['left']; ?></div>
  </div>
  <div class="same-height-element panel-panel grid-4">
    <div class="inside"><?php print $content['middle']; ?></div>
  </div>
  <div class="same-height-element panel-panel grid-4">
    <div class="inside"><?php print $content['right']; ?></div>
  </div>
</div>
