<?php

/**
 * Implements hook_panels_layouts().
 */
function custom_custom_landing_page_panels_layouts() {
  $items['custom_landing_page'] = array(
    'title' => t('Landing Page'),
    'category' => t('Custom'),
    'icon' => 'custom_landing_page.png',
    'theme' => 'custom_landing_page',
    'admin css' => '../omega_panels_admin_12.css',
    'regions' => array(
      'top' => t('Top'),
      'left' => t('Left side'),
      'right' => t('Right side'),
      'bottom' => t('Bottom'),
    ),
  );

  return $items;
}
