<?php
$with_sidebar = trim($content['right']) ? TRUE : FALSE;
?>
<div class="panel-display omega-grid custom-landing-page" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
  <?php if ($content['top']): ?>
    <div class="panel-panel grid-12">
      <div class="inside"><?php print $content['top']; ?></div>
    </div>
  <?php endif; ?>
  <?php if ($content['left']): ?>
    <div class="panel-panel <?php print $with_sidebar ? 'grid-8' : 'grid-12' ?>">
      <div class="inside"><?php print $content['left']; ?></div>
    </div>
  <?php endif; ?>
  <?php if ($with_sidebar): ?>
    <div class="panel-panel grid-4">
      <div class="inside"><?php print $content['right']; ?></div>
    </div>
  <?php endif; ?>
  <?php if ($content['bottom']): ?>
    <div class="panel-panel grid-12">
      <div class="inside"><?php print $content['bottom']; ?></div>
    </div>
  <?php endif; ?>
</div>
