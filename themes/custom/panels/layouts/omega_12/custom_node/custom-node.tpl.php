<?php
$with_sidebar = trim($content['right']) ? TRUE : FALSE;
?>
<div class="panel-display omega-grid custom-node" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
  <div class="panel-panel <?php print $with_sidebar ? 'grid-9' : 'grid-12' ?>">
    <div class="inside"><?php print $content['left']; ?></div>
  </div>
  <?php if ($with_sidebar): ?>
    <div class="panel-panel grid-3">
      <div class="inside"><?php print $content['right']; ?></div>
    </div>
  <?php endif; ?>
</div>
