<?php

/**
 * Implements hook_panels_layouts().
 */
function custom_custom_node_panels_layouts() {
  $items['custom_node'] = array(
    'title' => t('Omega-12 Node (9-3)'),
    'category' => t('Custom'),
    'icon' => 'custom_node.png',
    'theme' => 'custom_node',
    'admin css' => '../omega_panels_admin_12.css',
    'regions' => array(
      'left' => t('Left side'),
      'right' => t('Right side')
    ),
  );

  return $items;
}
