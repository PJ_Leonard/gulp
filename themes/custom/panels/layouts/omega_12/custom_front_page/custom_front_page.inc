<?php

/**
 * Implements hook_panels_layouts().
 */
function custom_custom_front_page_panels_layouts() {
  $items['custom_front_page'] = array(
    'title' => t('Custom Front Page'),
    'category' => t('Custom'),
    'theme' => 'custom_front_page',
    'icon' => 'custom_front_page.png',
    'admin css' => '../omega_panels_admin_12.css',
    'regions' => array(
      'top_left' => t('Top Left'),
      'top_right' => t('Top Right'),
      'middle' => t('Middle column'),
      'bottom_left' => t('Bottom Left'),
      'bottom_middle' => t('Bottom Middle'),
      'bottom_right' => t('Bottom Right'),
    ),
  );

  return $items;
}
