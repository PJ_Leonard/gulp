<?php
/**
 * @file
 * Display Suite Media object and body template.
 *
 * Available variables:
 *
 * Layout:
 * - $classes: String of classes that can be used to style this layout.
 * - $contextual_links: Renderable array of contextual links.
 * - $layout_wrapper: wrapper surrounding the layout.
 *
 * Regions:
 *
 * - $header: Rendered content for the "Header" region.
 * - $header_classes: String of classes that can be used to style the "Header" region.
 *
 * - $media_thumb: Rendered content for the "Media thumb" region.
 * - $media_thumb_classes: String of classes that can be used to style the "Media thumb" region.
 *
 * - $media_text: Rendered content for the "Media text" region.
 * - $media_text_classes: String of classes that can be used to style the "Media text" region.
 *
 * - $media_aside: Rendered content for the "Media aside" region.
 * - $media_aside_classes: String of classes that can be used to style the "Media aside" region.
 *
 * - $body: Rendered content for the "Body" region.
 * - $body_classes: String of classes that can be used to style the "Body" region.
 */

  // Add media classes so that we can apply the correct width to media items.
  $count = 0;
  $media_classes = '';
  if ($media_thumb) {
    $media_classes .= ' withThumb';
    $count++;
  }
  if ($media_text) {
    $media_classes .= ' withText';
    $count++;
  }
  if ($media_aside) {
    $media_classes .= ' withAside';
    $count++;
  }

  $media_classes .= ($count === 3) ? ' threeUp' : (($count === 2) ? ' twoUp' : ' oneUp');
?>
<<?php print $layout_wrapper; print $layout_attributes; ?> class="<?php print $classes;?> media-object-and-body clearfix">

  <!-- Needed to activate contextual links -->
  <?php if (isset($title_suffix['contextual_links'])): ?>
    <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>

    <?php if ($header): ?>
    <<?php print $header_wrapper; ?> class="ds-header<?php print $header_classes; ?>">
      <?php print $header; ?>
    </<?php print $header_wrapper; ?>>
    <?php endif; ?>

    <?php if ($media_thumb || $media_text || $media_aside): ?>
    <div class="ds-media <?php print $media_classes;?>">
      <?php if ($media_thumb): ?>
      <<?php print $media_thumb_wrapper; ?> class="ds-media-thumb<?php print $media_thumb_classes; ?>">
        <?php print $media_thumb; ?>
      </<?php print $media_thumb_wrapper; ?>>
      <?php endif; ?>

      <?php if ($media_text): ?>
      <<?php print $media_text_wrapper; ?> class="ds-media-text<?php print $media_text_classes; ?>">
        <?php print $media_text; ?>
      </<?php print $media_text_wrapper; ?>>
      <?php endif; ?>

      <?php if ($media_aside): ?>
      <<?php print $media_aside_wrapper; ?> class="ds-media-aside<?php print $media_aside_classes; ?>">
        <?php print $media_aside; ?>
      </<?php print $media_aside_wrapper; ?>>
      <?php endif; ?>
    </div>
    <?php endif; ?>

    <?php if ($body): ?>
    <<?php print $body_wrapper; ?> class="ds-body<?php print $body_classes; ?>">
      <?php print $body; ?>
    </<?php print $body_wrapper; ?>>
    <?php endif; ?>

</<?php print $layout_wrapper ?>>

<!-- Needed to activate display suite support on forms -->
<?php if (!empty($drupal_render_children)): ?>
  <?php print $drupal_render_children ?>
<?php endif; ?>
