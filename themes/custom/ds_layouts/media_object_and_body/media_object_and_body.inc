<?php

/**
 * @file
 * Display Suite Media object and body configuration.
 */

function ds_media_object_and_body() {
  return array(
    'label' => t('Media object and body'),
    'regions' => array(
      'header' => t('Header'),
      'media_thumb' => t('Media thumb'),
      'media_text' => t('Media text'),
      'media_aside' => t('Media aside'),
      'body' => t('Body'),
    ),
    // Uncomment if you want to include a CSS file for this layout (media_object_and_body.css)
    'css' => TRUE,
    // Uncomment if you want to include a preview for this layout (media_object_and_body.png)
    'image' => TRUE,
  );
}
