<?php
/**
 * @file
 * Display Suite Media object and body template.
 *
 * Available variables:
 *
 * Layout:
 * - $classes: String of classes that can be used to style this layout.
 * - $contextual_links: Renderable array of contextual links.
 * - $layout_wrapper: wrapper surrounding the layout.
 *
 * Regions:
 *
 * - $first: Rendered content for the "First" region.
 * - $first_classes: String of classes that can be used to style the "First" region.
 *
 * - $second: Rendered content for the "Second" region.
 * - $second_classes: String of classes that can be used to style the "Second" region.
 *
 * - $third: Rendered content for the "Third" region.
 * - $third_classes: String of classes that can be used to style the "Third" region.
 */
?>
<<?php print $layout_wrapper; print $layout_attributes; ?> class="<?php print $classes;?> three-bands clearfix">

  <!-- Needed to activate contextual links -->
  <?php if (isset($title_suffix['contextual_links'])): ?>
    <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>

    <?php if ($first): ?>
    <<?php print $first_wrapper; ?> class="ds-first<?php print $first_classes; ?>">
      <?php print $first; ?>
    </<?php print $first_wrapper; ?>>
    <?php endif; ?>

    <?php if ($second): ?>
    <<?php print $second_wrapper; ?> class="ds-second<?php print $second_classes; ?>">
      <?php print $second; ?>
    </<?php print $second_wrapper; ?>>
    <?php endif; ?>

    <?php if ($third): ?>
    <<?php print $third_wrapper; ?> class="ds-third<?php print $third_classes; ?>">
      <?php print $third; ?>
    </<?php print $third_wrapper; ?>>
    <?php endif; ?>


</<?php print $layout_wrapper ?>>

<!-- Needed to activate display suite support on forms -->
<?php if (!empty($drupal_render_children)): ?>
  <?php print $drupal_render_children ?>
<?php endif; ?>
