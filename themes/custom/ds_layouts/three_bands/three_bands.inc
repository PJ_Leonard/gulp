<?php

/**
 * @file
 * Display Suite Media object and body configuration.
 */

function ds_three_bands() {
  return array(
    'label' => t('Three bands'),
    'regions' => array(
      'first' => t('First'),
      'second' => t('Second'),
      'third' => t('Third'),
    ),
    // Uncomment if you want to include a CSS file for this layout (media_object_and_body.css)
    //'css' => TRUE,
    // Uncomment if you want to include a preview for this layout (media_object_and_body.png)
    //'image' => TRUE,
  );
}
