<?php
/**
 * Newsletter template file.  Copy this file to the active theme directory to 
 * override it.  You can create a different template for each node type by 
 * copying this to the active theme directory and renaming it enews-{type}.tpl.php
 * where [type] is the machine readable name of the node type.
 * 
 * NOTE: The base theme "enews.tpl.php" MUST exist in the same directory as any
 * template suggestions.  Therefore one MUST include a copy of enews.tpl.php in
 * the active theme directory to use node type specific template overrides 
 * (enews-page.tpl.php).
 * 
 * 
 * Available variables.
 * $node - the node object
 * $content - the rendered node object
 * All regions from the current active theme are available as well.
 * @see path/to/active/theme.info
 * $header - header region from theme
 * $footer - footer region from theme
 * $closure - closure region from theme
 ******* NEW VARIABLES FOR COLOR *******
 * $base_color from node color picker
 * $accent_color 
 * $highlight_color
 * $link_color 
 * feel free to use or ignore as needed
 ***************************************
 */
global $base_url, $theme_path;
$abs = $base_url . '/' . $theme_path;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <title><?php print $title . ' : ' . $site_name; ?></title>
    <style>
<?php 
// inline styles here get emogrified, and override newsletter.css styles.
// this lets us leverage the colorpicker stuff for dynamic colored newsletters
?>
html { 
	color: <?php echo $accent_color ?>;
}

body {
	color:<?php echo $accent_color ?>;
	background:<?php echo $base_color ?>;
}

.wrap {
  background:<?php echo $base_color ?>;
}

table,th,tr,td {
	background:<?php echo $base_color ?>;
}

a {
	color:<?php echo $link_color ?>;
}

a.button {
	color:<?php echo $base_color ?>;
	background:<?php echo $link_color ?>;
}

h1,
.field-name-field-newsletter-topic {
	background:<?php echo $accent_color ?>;
	color:<?php echo $base_color ?>;
}

.node-newsletter .field-name-field-image,
.node-newsletter .field-name-field-newsletter-image,
.node-newsletter .field-name-field-newsletter-link,
.node-newsletter .field-name-body { 
  background:<?php echo $highlight_color ?>; 
}

#footer td {
	background:<?php echo $accent_color ?>;
	color:<?php echo $base_color ?>;
}

#footer td a {
	color:<?php echo $base_color ?>;
}

    </style>
  </head>
  <body id="enews-tpl">
	<div class="wrap">
    <table id="newsletter-table">
      <tr>
        <td colspan="2">
		  <div id="logo">
          <a href="<?php print $base_url; ?>">
            <img src="<?php echo $abs; ?>/logo.png" alt="Logo" />
          </a>
		  </div>
        </td>
      </tr>
      <tr>
        <td colspan="2"><div class="field-name-field-newsletter-topic"><?php print $node->field_newsletter_topic['und'][0]['value']; ?></div></td>
      </tr>
      <tr>
        <td colspan="2"><h1 id="newsletter-title"><?php print $title; ?></h1></td>
      </tr>
      <tr>
        <td colspan="2">
          <?php print $content; ?>
        </td>
      </tr>
      <tr id="footer">
        <td>
			<div class="footer-links"><a href="/home" target="_blank">Home</a> | <a href="/locations" target="_blank">Locations</a> | <a href="/contact" target="_blank">Contact</a> | <a href="/support" target="_blank">Support</a></div>
        </td>
		<td>
			<div class="social"><a href="https://www.facebook.com/"><img src="<?php print $abs; ?>/images/newsletter-fb.png" alt="Follow us on Facebook" /></a> <a href="https://www.twitter.com/"><img src="<?php print $abs; ?>/images/newsletter-tw.png" alt="Follow us on Twitter" /></a></div>
		</td>
      </tr>
      <tr id="footer-second">
        <td colspan="2">
		<p><a href="<?php print $base_url; ?>">LOREMIPSUM.ORG</a></p>
	</td>
      </tr>
    </table>
</div>
  </body>
</html>
