<div<?php print $attributes; ?>>
  <div<?php print $content_attributes; ?>>
    <?php if (isset($messages)) { print $messages; } ?>
    <div class="lining">
      <a id="main-content"></a>

      <!-- moved title to region--preface_first.tpl.php -->

      <?php if ($tabs && !empty($tabs['#primary'])): ?><div class="tabs clearfix"><?php print render($tabs); ?></div><?php endif; ?>
      <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
      <?php print $content; ?>
      <?php if ($feed_icons): ?><div class="feed-icon clearfix"><?php print $feed_icons; ?></div><?php endif; ?>
    </div>
  </div>
</div>
