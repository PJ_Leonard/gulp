<div<?php print $attributes; ?>>
  <div<?php print $content_attributes; ?>>
    <?php print render($title_prefix); ?>
    <?php print render($title_suffix); ?>
    <?php if ($title || $title_hidden || $content): ?>
    <div class="content <?php if ($field_block) { print ' two-cols'; } ?>">
      <?php if ($field_block): ?>
      <div class="content--header">
      <?php endif; ?>
      <?php if ($type): ?>
        <div class="meta--type"><?php print $type; ?></div>
      <?php endif; ?>
      <?php if ($title): ?>
        <?php if ($title_hidden): ?>
          <div class="element-invisible">
        <?php endif; ?>
        <h1 class="title" id="page-title"><span><?php print $title; ?></span></h1>
        <?php if ($title_hidden): ?>
          </div>
        <?php endif; ?>
      <?php endif; ?>
      <?php if ($byline): ?>
        <div class="meta--byline"><?php print $byline; ?></div>
      <?php endif; ?>
      <?php if ($field_block): ?>
      </div>
      <?php endif; ?>
      <?php if ($field_block): ?>
      <div class="content--left">
      <?php print $content; ?>
      </div>
      <?php endif; ?>
      <?php if ($field_block): ?>
      <div class="content--right">
      <?php endif; ?>
      <?php if ($field_block): ?>
      <?php print ($field_block); ?>
      <?php else: ?>
      <?php print $content; ?>
      <?php endif; ?>
      <?php if ($field_block): ?>
      </div>
      <?php endif; ?>
    </div>
    <?php endif; ?>
  </div>
</div>