<?php

/**
 * @file
 * This template is used to print a single grouping in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $grouping: The grouping instruction.
 * - $grouping_level: Integer indicating the hierarchical level of the grouping.
 * - $rows: The rows contained in this grouping.
 * - $title: The title of this grouping.
 * - $content: The processed content output that will normally be used.
 */
  $group_class = '';
  if ($title) {
    $group_class = 'group-'.str_replace(' ', '-', strtolower($title));
  }
?>
<div class="view-grouping">
  <?php if ($title): ?><div class="view-grouping-header <?php print $group_class; ?>"><h3><?php print $title; ?></h3></div><?php endif; ?>
  <div class="view-grouping-content <?php print $group_class; ?>">
    <?php print $content; ?>
  </div>
</div>