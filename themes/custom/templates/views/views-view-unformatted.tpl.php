<?php

/**
 * @file
 * Custom simple view template to display a list of rows and to group headings and rows separately.
 *
 * @ingroup views_templates
 */
  $group_class = '';
  if ($title) {
    $group_class = 'group-'.preg_replace('/-{2,}/','' ,drupal_clean_css_identifier(strtolower(strip_tags($title))));
  }
?>
<?php if (!empty($title)): ?>
  <div class="group-header <?php print $group_class; ?>">
    <h3><?php print $title; ?></h3>
  </div>
<?php endif; ?>
<?php if (!empty($title)): ?><div class="group-content <?php print $group_class; ?>"><?php endif; ?>
<?php foreach ($rows as $id => $row): ?>
  <div<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .'"';  } ?>>
    <?php print $row; ?>
  </div>
<?php endforeach; ?>
<?php if (!empty($title)): ?></div><?php endif; ?>