
<div class="<?php print $classes . preg_replace('/-{2,}/','' ,drupal_clean_css_identifier(strtolower(strip_tags($legend))));; ?>"<?php print $attributes; ?>>

  <?php foreach ($fieldset_fields as $name => $field): ?>
    <?php print @$field->separator . $field->wrapper_prefix . $field->label_html . $field->content . $field->wrapper_suffix; ?>
  <?php endforeach; ?>

</div>

