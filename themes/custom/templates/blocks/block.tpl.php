<div<?php print $attributes; ?>>
  <?php if (isset($messages_icon)) { print $messages_icon; } ?>
  <div class="block-inner clearfix">
    <?php print render($title_prefix); ?>
    <?php if ($block->subject): ?>
      <h2<?php print $title_attributes; ?>><span><?php print $block->subject; ?></span></h2>
    <?php endif; ?>
    <?php print render($title_suffix); ?>

    <div<?php print $content_attributes; ?>>
      <?php print $content ?>
    </div>
  </div>
</div>
