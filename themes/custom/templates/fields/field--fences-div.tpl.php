<?php
/**
 * @file field--fences-div.tpl.php
 * Wrap each field value in the <div> element.
 *
 * @see http://developers.whatwg.org/grouping-content.html#the-div-element
 */
?>
<div class="field <?php if (isset($bgs)) { print 'is_bgs '; } ?><?php print $classes; ?>">
<?php if ($element['#label_display'] == 'inline'): ?>
  <span class="field-label"<?php print $title_attributes; ?>>
    <?php print $label; ?>:
  </span>
<?php elseif ($element['#label_display'] == 'above'): ?>
  <h3 class="field-label field-lable-above"<?php print $title_attributes; ?>>
    <span class="text"><?php print $label; ?></span>
  </h3>
<?php endif; ?>

<?php foreach ($items as $delta => $item): ?>
  <div class="field-contents"<?php print $attributes; ?> <?php if (isset($bgs)) { print('style="background-image: url('. $bgs[$delta] .');"'); } ?>>
    <?php print render($item); ?>
  </div>
<?php endforeach; ?>
</div>
