<?php
/**
 * @file field--fences-span.tpl.php
 * Wrap each field value in the <span> element.
 *
 * @see http://developers.whatwg.org/text-level-semantics.html#the-span-element
 */
?>
<div class="field <?php print $classes; ?>">
<?php if ($element['#label_display'] == 'inline'): ?>
  <span class="field-label"<?php print $title_attributes; ?>>
    <?php print $label; ?>:
  </span>
<?php elseif ($element['#label_display'] == 'above'): ?>
  <h3 class="field-label field-lable-above"<?php print $title_attributes; ?>>
    <span class="text"><?php print $label; ?></span>
  </h3>
<?php endif; ?>

<div class="field-contents"<?php print $attributes; ?>>
<?php foreach ($items as $delta => $item): ?>
  <span class="<?php print $classes; ?>"<?php print $attributes; ?>>
    <?php print render($item); ?>
  </span>
<?php endforeach; ?>
</div>
</div>
