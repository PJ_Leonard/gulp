<?php
/**
 * @file field--fences-p.tpl.php
 * Wrap each field value in the <p> element.
 *
 * @see http://developers.whatwg.org/grouping-content.html#the-p-element
 */
?>
<div class="field <?php print $classes; ?>">
<?php if ($element['#label_display'] == 'inline'): ?>
  <span class="field-label"<?php print $title_attributes; ?>>
    <?php print $label; ?>:
  </span>
<?php elseif ($element['#label_display'] == 'above'): ?>
  <h3 class="field-label field-lable-above"<?php print $title_attributes; ?>>
    <span class="text"><?php print $label; ?></span>
  </h3>
<?php endif; ?>
<div class="field-contents"<?php print $attributes; ?>>
<?php foreach ($items as $delta => $item): ?>
  <p class="<?php print $classes; ?>"<?php print $attributes; ?>>
    <?php print render($item); ?>
  </p>
<?php endforeach; ?>
<div>
</div>