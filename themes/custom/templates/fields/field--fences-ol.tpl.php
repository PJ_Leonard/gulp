<?php
/**
 * @file field--fences-ol.tpl.php
 * Wrap each field value in the <li> element and all of them in the <ol> element.
 *
 * @see http://developers.whatwg.org/grouping-content.html#the-ol-element
 */
?>
<div class="field <?php print $classes; ?>">
<?php if ($element['#label_display'] == 'inline'): ?>
  <span class="field-label"<?php print $title_attributes; ?>>
    <?php print $label; ?>:
  </span>
<?php elseif ($element['#label_display'] == 'above'): ?>
  <h3 class="field-label field-lable-above"<?php print $title_attributes; ?>>
    <span class="text"><?php print $label; ?></span>
  </h3>
<?php endif; ?>
<ol class="field-contents"<?php print $attributes; ?>><?php foreach ($items as $delta => $item): ?><li<?php print $item_attributes[$delta]; ?>><?php print render($item); ?></li><?php endforeach; ?></ol>
</div>