<?php

/**
 * @file
 * Display Suite 1 column template.
 */
?>

<?php $linked = array('cvrp_survey'); //These content types are fully wrapped in a link ?> 

<<?php print $ds_content_wrapper; print $layout_attributes; ?> class="ds-1col <?php print $classes;?> clearfix">

  <?php if (isset($title_suffix['contextual_links'])): ?>
  <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>
  <?php if (in_array($node->type, $linked)): ?>
  <a href="<?php echo url('node/' . $nid); ?>">
  <?php endif; ?>
  <?php print $ds_content; ?>
  <?php if (in_array($node->type, $linked)): ?>
  </a>
  <?php endif; ?>
</<?php print $ds_content_wrapper ?>>

<?php if (!empty($drupal_render_children)): ?>
  <?php print $drupal_render_children ?>
<?php endif; ?>
