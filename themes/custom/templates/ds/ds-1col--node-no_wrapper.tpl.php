<?php

/**
 * @file
 * Display Suite 1 column template.
 * No wrapper has no wrapper, obviously.
 */
?>

<?php print $ds_content; ?>

<?php if (!empty($drupal_render_children)): ?>
  <?php print $drupal_render_children ?>
<?php endif; ?>
