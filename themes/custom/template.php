<?php

/**
 * @file
 * This file is empty by default because the base theme chain (Alpha & Omega) provides
 * all the basic functionality. However, in case you wish to customize the output that Drupal
 * generates through Alpha & Omega this file is a good place to do so.
 *
 * Alpha comes with a neat solution for keeping this file as clean as possible while the code
 * for your subtheme grows. Please read the README.txt in the /preprocess and /process subfolders
 * for more information on this topic.
 */


function custom_breadcrumb($vars) {
  $breadcrumb = $vars['breadcrumb'];

  if (!empty($breadcrumb)) {
    // Provide a navigational heading to give context for breadcrumb links to
    // screen-reader users. Make the heading invisible with .element-invisible.
    $output = '<h2 class="element-invisible">' . t('You are here') . '</h2>';

    $output .= '<div class="breadcrumb">' . implode(' › ', $breadcrumb) . '</div>';
    return $output;
  }
}

function custom_link($vars) {
  $text = $vars['text'];
  preg_match('/^<img\s+.*?\s\/>\s+/', $text, $img);
  $img = count($img) ? $img[0] : '';
  if ($img) {
    $text = str_replace($img, '', $text);
  }

  return '<a href="' . check_plain(url($vars['path'], $vars['options'])) . '"' . drupal_attributes($vars['options']['attributes']) . '>'.$img.'<span class="text">' . ($vars['options']['html'] ? $text : check_plain($text)) . '</span></a>';
}


function custom_file_link($vars) {
  $file = $vars['file'];
  // $vars['icon_directory'] = path_to_theme().'/images/file_icons';
  $icon_directory = $vars['icon_directory'];

  $url = file_create_url($file->uri);

  // Human-readable names, for use as text-alternatives to icons.
  $mime_name = array(
    'application/msword' => t('Microsoft Office document icon'),
    'application/vnd.ms-excel' => t('Office spreadsheet icon'),
    'application/vnd.ms-powerpoint' => t('Office presentation icon'),
    'application/pdf' => t('PDF icon'),
    'video/quicktime' => t('Movie icon'),
    'audio/mpeg' => t('Audio icon'),
    'audio/wav' => t('Audio icon'),
    'image/jpeg' => t('Image icon'),
    'image/png' => t('Image icon'),
    'image/gif' => t('Image icon'),
    'application/zip' => t('Package icon'),
    'text/html' => t('HTML icon'),
    'text/plain' => t('Plain text icon'),
    'application/octet-stream' => t('Binary Data'),
  );

  $mimetype = file_get_mimetype($file->uri);

  $icon = theme('file_icon', array(
    'file' => $file,
    'icon_directory' => $icon_directory,
    'alt' => !empty($mime_name[$mimetype]) ? $mime_name[$mimetype] : t('File'),
  ));

  // Set options as per anchor format described at
  // http://microformats.org/wiki/file-format-examples
  $options = array(
    'attributes' => array(
      'type' => $file->filemime . '; length=' . $file->filesize,
    ),
    'html' => TRUE,
  );

  // Use the description as the link text if available.
  if (empty($file->description)) {
    $link_text = $file->filename;
  }
  else {
    $link_text = $file->description;
    $options['attributes']['title'] = check_plain($file->filename);
  }

  return '<span class="file">' . ' ' . l($icon . ' ' . $link_text, $url, $options) . '</span>';
}

function custom_menu_link($vars) {
  $element = $vars['element'];
  $sub_menu = '';

  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }
  foreach($element['#below'] as $key=>$item) {
    //the child menu items' keys are their mlid, which is numeric
    //most other items in this array will not have numeric keys
    if(is_numeric($key)) {
      if (!empty($item['#below'])) {
        $element['#attributes']['class'][] = 'hasMenus';
      }
    }
  }

  $element['#attributes']['class'][] = 'menu-title-'.drupal_clean_css_identifier(strtolower(strip_tags($element['#title'])));

  $element['#localized_options']['html'] = TRUE;
  $linktext = '<span class="menu-item-inner">' . $element['#title'] . '</span>';

  $output = l($linktext, $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

function custom_preprocess_html(&$vars) {
  // Drupal core is supposed to do this, but incompatible with omega.
  // And omega doesn't do this on our behalf.
  // Super annoying to fix the improper classes.
  foreach (array('no-sidebars', 'two-sidebars', 'left-sidebar', 'right-sidebar', 'one-sidebar', 'sidebar-first', 'sidebar-second') as $key) {
    if ($i = array_search($key, $vars['classes_array'])) {
      unset($vars['classes_array'][$i]);
    }
  }

  if (empty($vars['page']['content']['content']['sidebar_first'])
  && empty($vars['page']['content']['content']['sidebar_second'])) {
    $vars['classes_array'][] = 'no-sidebars';
  }
  elseif (!empty($vars['page']['content']['content']['sidebar_first'])
  && !empty($vars['page']['content']['content']['sidebar_second'])) {
    $vars['classes_array'][] = 'two-sidebars';
  }
  elseif (!empty($vars['page']['content']['content']['sidebar_first'])) {
    $vars['classes_array'][] = 'left-sidebar';
  }
  elseif (!empty($vars['page']['content']['content']['sidebar_second'])) {
    $vars['classes_array'][] = 'right-sidebar';
  }
}

function custom_preprocess_page(&$vars) {
  $content = &$vars['page']['content']['content'];
  // If user doesn't have permission to change own username, hide tabs from them always.
  if (!user_access('change own username')) {
    $vars['tabs']['#access'] = FALSE;
  }

  // This disables message-printing on ALL page displays
  $vars['show_messages'] = FALSE;

  // @see custom_preprocess_html
  if (empty($content['sidebar_first'])
  && empty($content['sidebar_second'])) {
    drupal_static('body_sidebar_classes', 'no-sidebars');
  }
  elseif (!empty($content['sidebar_first'])
  && !empty($content['sidebar_second'])) {
    drupal_static('body_sidebar_classes', 'two-sidebars');
  }
  elseif (!empty($content['sidebar_first'])) {
    drupal_static('body_sidebar_classes', 'left-sidebar');
  }
  elseif (!empty($content['sidebar_second'])) {
    drupal_static('body_sidebar_classes', 'right-sidebar');
  }

  $path = drupal_strtolower(drupal_get_path_alias($_GET['q']));
  $pat = 'donate*';
  if (drupal_match_path($path, $pat) || drupal_match_path($_GET['q'], $pat)) {
    drupal_add_css(path_to_theme().'/css/donate.css.less');
  }

  if (drupal_is_front_page()) { 
    if (isset($content['content']['system_main']['pager'])) {
      unset($content['content']['system_main']['pager']); // and the pager
    }
    if (isset($content['content']['system_main']['default_message'])) {
      unset($content['content']['system_main']['default_message']); // and the default front page message
    }
  }

  // Toggle Admin
  if (user_access('access administration menu')) {
    // don't show toggle admin button to non-admins
    drupal_add_js(path_to_theme().'/js/toggleAdmin.js', array('group' => JS_THEME, 'weight' => 10));
    drupal_add_css(path_to_theme().'/css/toggleAdmin.css', array('group' => CSS_THEME, 'weight' => 100));
  }
  // need to update styles after using drupal_add_css or they won't show up.
  // probably need to update javascript too...
  $vars['styles'] = drupal_get_css();
}

/**
 * hook_preprocess_node()
 */
function custom_preprocess_node(&$vars) {
  switch($vars['type']) {
    case 'cvrp_survey':
      if ($vars['view_mode'] === 'teaser') {
        // See: /eng/program-reports
        foreach ($vars['content'] as &$field) {
          if (isset($field['#field_type']) && $field['#field_type'] === 'ds') {
            foreach ($field as $delta => &$field_item) {
              if (is_numeric($delta)) {
                if (strpos($field_item['#markup'], '<') !== 0) {
                  $field_item['#markup'] =
                    '<span class="text">'
                    . $field_item['#markup']
                  . '<span>';
                } elseif (strpos($field_item['#markup'], '<h') === 0) {
                  $field_item['#markup'] = preg_replace('/^<h(\d)>(.*?)<\/h.>/', '<h$1><span class="text">$2</span></h$1>', $field_item['#markup']);
                }
              }
            }
          }
        }
      }
    break;
  }
}

/**
 * hook_preprocess_image_style()
 * @see theme_image_style
 */
function custom_preprocess_image_style(&$vars) {
  // Add a class for the image style.
  if (empty($vars['attributes'])) {
    $vars['attributes'] = array();
  }
  if (empty($vars['attributes']['class'])) {
    $vars['attributes']['class'] = array();
  }
  $vars['attributes']['class'][] = 'image-style-' . $vars['style_name'];
}

/**
 * hook_preprocess_image_formatter
 * @see theme_image_formatter
 */
function custom_preprocess_image_formatter(&$vars) {
  // Add a class when using the default image
  if (!empty($vars['item']['is_default'])) {
    if (empty($vars['item']['attributes'])) {
      $vars['item']['attributes'] = array();
    }
    if (empty($vars['item']['attributes']['class'])) {
      $vars['item']['attributes']['class'] = array();
    }
    $vars['item']['attributes']['class'][] = 'default-image';
  }
}

function custom_preprocess_zone(&$vars) {
  $vars['theme_hook_suggestions'][] = 'zone';
}

/**
 * Implements hook_process_region().
 */
function custom_alpha_preprocess_region(&$vars) {
  $elements = $vars['elements'];
  $region = $elements['#region'];
  $theme = alpha_get_theme();
  // should check to make sure this is actually a node.
  $node = menu_get_object();

  // Set some vars
  switch ($region) {
    // See: region--preface_first.tpl.php
    case 'preface_first':
      $vars['title_prefix'] = $theme->page['title_prefix'];
      $vars['title'] = $theme->page['title'];
      $vars['title_suffix'] = $theme->page['title_suffix'];
      $vars['title_hidden'] = $theme->page['title_hidden'];
      $vars['type'] = FALSE;
      $vars['byline'] = FALSE;
      $vars['intro'] = FALSE;
      $vars['field_block'] = FALSE;

      // get url of banner images (image style)
      // add the first as a background image
      // and add all as a data attribute (json) to randomize with jquery
      if (isset($node->field_banner)) {
        $field_banner = $node->field_banner;
        $vars['banner_images'] = array();
        if (!empty($field_banner)) {
          $imgs = $field_banner['und'][0]['entity']->field_banner_images['und'];
          foreach ($imgs as $key => $img) {
            $style_name = 'banner';
            $vars['banner_images'][] = array(
              'img' => image_style_url($style_name, file_uri_target($img['uri'])),
              'style' => $style_name);
          }
          // Add the first item to attributes
          $vars['attributes_array']['class'][] = $vars['banner_images'][0]['style'];
          $vars['attributes_array']['style'] = array('background-image: url('. $vars['banner_images'][0]['img'].');');
          $vars['attributes_array']['data-banner-images'] = json_encode($vars['banner_images'], JSON_UNESCAPED_SLASHES);
        }
      }
    break;
    case 'content':
      $vars['messages'] = theme('status_messages');
    break;
  }

  // Allows custom region templates per content type
  if (isset($node->type)) {
    $node_wrapper = entity_metadata_wrapper('node', $node);
    $vars['theme_hook_suggestions'][] = 'region__' . $region . '__' . $node->type;
    $vars['attributes_array']['class'][] = 'region-' . $region . '-'. $node->type;

    switch ($node->type) {
      // Can set some node-type-specific vars here
      case 'news':
        $vars['type'] = t('News');
        $vars['byline'] = format_date($node_wrapper->created->value(), 'medium');
      break;
      case 'landing_page':
        if (isset($node->field_block) && !empty($node->field_block)) {
          // Moddelta is from block reference module
          $moddelta = explode(':', $node->field_block['und'][0]['moddelta']);
          $block = block_load($moddelta[0], $moddelta[1]);
          $render_array = _block_get_renderable_array(_block_render_blocks(array($block)));
          $vars['field_block'] = render($render_array);
        }
      break;
    }
  }
}

/**
 * Theme function for status messages.
 *
 * @ingroup themable
 */
function custom_status_messages($vars) {
  $display = $vars['display'];
  $output = '';

  $status_heading = array(
    'status' => t('Status message'),
    'error' => t('Error message'),
    'warning' => t('Warning message'),
    'success' => t('Success message')
  );

  foreach (drupal_get_messages($display) as $type => $messages) {
    $icon_id = '#info';

    switch ($type) {
      case 'success':
        $icon_id = '#success';
      break;
      case 'warning':
      case 'warning cvrp-eligibility':
      case 'error':
        $icon_id = '#caution-sign';
      break;
      case 'status':
      default:
        $icon_id = '#info';
      break;
    }

    $icon  = '<b class="status-icon"><svg class="icon--'.$type.'"><use xlink:href="'.$icon_id.'"></use></svg></b>';

    $output .= "<div class=\"messages $type\">\n$icon\n<div class=\"messages--inner\">\n";
    if (!empty($status_heading[$type])) {
      $output .= '<h2 class="element-invisible">' . $status_heading[$type] . "</h2>\n";
    }
    if (count($messages) > 1) {
      $output .= " <ul>\n";
      foreach ($messages as $message) {
        $output .= '  <li>' . $message . "</li>\n";
      }
      $output .= " </ul>\n";
    }
    else {
      $output .= reset($messages);
    }
    $output .= "</div>\n</div>\n";
  }
  return $output;
}

function custom_preprocess_block(&$vars) {
  // Make sure that this is a custom block, and not a block that is provided by
  // a module.
  if ($vars['block']->module == 'block') {
    // Load the block information.
    $info = block_custom_block_get($vars['block']->delta);
    // Add block description to the list of classes.
    $vars['attributes_array']['class'][] = drupal_html_class($info['info']);
  }
  // Svg icons for faux messages block
  if (in_array('messages', $vars['classes_array'])) {
    $type = 'status';
    $icon_id = '#info';

    if (in_array('success', $vars['classes_array'])) {
      $icon_id = '#success';
    }
    else if (in_array('warning', $vars['classes_array'])) {
      $icon_id = '#caution-sign';
    }
    else if (in_array('error', $vars['classes_array'])) {
      $icon_id = '#caution-sign';
    }

    $vars['messages_icon'] = '<b class="status-icon"><svg class="icon--'.$type.'"><use xlink:href="'.$icon_id.'"></use></svg></b>';
  }

  // Menu Block
  if ($vars['block']->module == 'menu_block') {
    if ($vars['block']->region == 'menu') {
      // Put title text into a data attribute on the block
      // for easy javascript availability
      // and hide the title with .element-invisible;
      $vars['attributes_array']['data-toggle-text'] = $vars['block']->title;
      $vars['title_attributes_array']['class'][] = 'element-invisible';
    }
  }
}

function custom_form_element_label($vars) {
  $element = $vars['element'];
  // This is also used in the installer, pre-database setup.
  $t = get_t();

  // If title and required marker are both empty, output no label.
  if ((!isset($element['#title']) || $element['#title'] === '') && empty($element['#required'])) {
    return '';
  }

  // If the element is required, a required marker is appended to the label.
  $required = !empty($element['#required']) ? theme('form_required_marker', array('element' => $element)) : '';

  $title = filter_xss_admin($element['#title']);

  $attributes = array();
  // Style the label as class option to display inline with the element.
  if ($element['#title_display'] == 'after') {
    $attributes['class'] = 'option';
  }
  // Show label only to screen readers to avoid disruption in visual flows.
  elseif ($element['#title_display'] == 'invisible') {
    $attributes['class'] = 'element-invisible';
  }

  if (!empty($element['#id'])) {
    $attributes['for'] = $element['#id'];
  }

  // The leading whitespace helps visually separate fields from inline labels.
  return ' <label' . drupal_attributes($attributes) . '><span class="text">' . $t('!title !required', array('!title' => $title, '!required' => $required)) . "</span></label>\n";
}

// MA Default pager Paging right
function custom_pager($vars) {
  $tags = $vars['tags'];
  $element = $vars['element'];
  $parameters = $vars['parameters'];
  $quantity = $vars['quantity'];
  global $pager_page_array, $pager_total;

  // Calculate various markers within this pager piece:
  // Middle is used to "center" pages around the current page.
  $pager_middle = ceil($quantity / 2);
  // current is the page we are currently paged to
  $pager_current = $pager_page_array[$element] + 1;
  // first is the first page listed by this pager piece (re quantity)
  $pager_first = $pager_current - $pager_middle + 1;
  // last is the last page listed by this pager piece (re quantity)
  $pager_last = $pager_current + $quantity - $pager_middle;
  // max is the maximum page number
  $pager_max = $pager_total[$element];
  // End of marker calculations.

  // Prepare for generation loop.
  $i = $pager_first;
  if ($pager_last > $pager_max) {
    // Adjust "center" if at end of query.
    $i = $i + ($pager_max - $pager_last);
    $pager_last = $pager_max;
  }
  if ($i <= 0) {
    // Adjust "center" if at start of query.
    $pager_last = $pager_last + (1 - $i);
    $i = 1;
  }
  // End of generation loop preparation.

  $li_first = theme('pager_first', array('text' => (isset($tags[0]) ? $tags[0] : t('« first')), 'element' => $element, 'parameters' => $parameters));
  $li_previous = theme('pager_previous', array('text' => (isset($tags[1]) ? $tags[1] : t('‹ previous')), 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
  $li_next = theme('pager_next', array('text' => (isset($tags[3]) ? $tags[3] : t('next ›')), 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
  $li_last = theme('pager_last', array('text' => (isset($tags[4]) ? $tags[4] : t('last »')), 'element' => $element, 'parameters' => $parameters));

  if ($pager_total[$element] > 1) {

    // if ($li_first) {
    //   $items[] = array(
    //     'class' => array('pager-first'),
    //     'data' => $li_first,
    //   );
    // }
    if ($li_previous) {
      $items[] = array(
        'class' => array('pager-previous'),
        'data' => $li_previous,
      );
    }

    // When there is more than one page, create the pager list.
    if ($i != $pager_max) {
      if ($i > 1) {
        $items[] = array(
          'class' => array('pager-ellipsis'),
          'data' => '…',
        );
      }
      // Now generate the actual pager piece.
      for (; $i <= $pager_last && $i <= $pager_max; $i++) {
        if ($i < $pager_current) {
          $items[] = array(
            'class' => array('pager-item'),
            'data' => theme('pager_previous', array('text' => $i, 'element' => $element, 'interval' => ($pager_current - $i), 'parameters' => $parameters)),
          );
        }
        if ($i == $pager_current) {
          $items[] = array(
            'class' => array('pager-current'),
            'data' => '<span>'.$i.'</span>',
          );
        }
        if ($i > $pager_current) {
          $items[] = array(
            'class' => array('pager-item'),
            'data' => theme('pager_next', array('text' => $i, 'element' => $element, 'interval' => ($i - $pager_current), 'parameters' => $parameters)),
          );
        }
      }
      if ($i < $pager_max) {
        $items[] = array(
          'class' => array('pager-ellipsis'),
          'data' => '…',
        );
      }
    }

    // End generation.
    if ($li_next) {
      $items[] = array(
        'class' => array('pager-next'),
        'data' => $li_next,
      );
    }
    // if ($li_last) {
    //   $items[] = array(
    //     'class' => array('pager-last'),
    //     'data' => $li_last,
    //   );
    // }

    $output = '<div class="pager-bar">';
    $output .= theme('item_list', array(
      'items' => $items,
      'attributes' => array('class' => array('pager', 'pager-default')),
    ));
    $output.= '</div>';

    return $output;
  }
}

function custom_views_mini_pager($vars) {
  global $pager_page_array, $pager_total;

  $tags = $vars['tags'];
  $element = $vars['element'];
  $parameters = $vars['parameters'];

  // current is the page we are currently paged to
  $pager_current = $pager_page_array[$element] + 1;
  // max is the maximum page number
  $pager_max = $pager_total[$element];
  // End of marker calculations.

  if ($pager_total[$element] > 1) {

    $li_previous = theme('pager_previous',
    array(
      'text' => (isset($tags[1]) ? $tags[1] : t('‹‹')),
      'element' => $element,
      'interval' => 1,
      'parameters' => $parameters,
    )
    );
    if (empty($li_previous)) {
      $li_previous = "&nbsp;";
    }

    $li_next = theme('pager_next',
    array(
      'text' => (isset($tags[3]) ? $tags[3] : t('››')),
      'element' => $element,
      'interval' => 1,
      'parameters' => $parameters,
    )
    );

    if (empty($li_next)) {
      $li_next = "&nbsp;";
    }

    $items[] = array(
      'class' => array('pager-previous'),
      'data' => $li_previous,
    );

    $items[] = array(
      'class' => array('pager-current'),
      'data' => t('@current of @max', array('@current' => $pager_current, '@max' => $pager_max)),
    );

    $items[] = array(
      'class' => array('pager-next'),
      'data' => $li_next,
    );

    $output = '<div class="pager-bar">';
    $output .= theme('item_list', array(
      'items' => $items,
      'title' => NULL,
      'type' => 'ul',
      'attributes' => array('class' => array('pager')),
    ));

    return $output;
  }
}

function custom_pager_previous($vars) {
  $text = $vars['text'];
  $element = $vars['element'];
  $interval = $vars['interval'];
  $parameters = $vars['parameters'];
  global $pager_page_array;
  $output = '';

  // If we are anywhere but the first page
  if ($pager_page_array[$element] > 0) {
    $page_new = pager_load_array($pager_page_array[$element] - $interval, $element, $pager_page_array);

    // If the previous page is the first page, mark the link as such.
    if ($page_new[$element] == 0) {
      $output = theme('pager_first', array('text' => $text, 'element' => $element, 'parameters' => $parameters));
    }
    // The previous page is not the first page.
    else {
      $output = theme('pager_link', array('text' => $text, 'page_new' => $page_new, 'element' => $element, 'parameters' => $parameters));
    }
  } else {
    $output = '<span>&nbsp;</span>';
  }

  return $output;
}

function custom_pager_next($vars) {
  $text = $vars['text'];
  $element = $vars['element'];
  $interval = $vars['interval'];
  $parameters = $vars['parameters'];
  global $pager_page_array, $pager_total;
  $output = '';

  // If we are anywhere but the last page
  if ($pager_page_array[$element] < ($pager_total[$element] - 1)) {
    $page_new = pager_load_array($pager_page_array[$element] + $interval, $element, $pager_page_array);
    // If the next page is the last page, mark the link as such.
    if ($page_new[$element] == ($pager_total[$element] - 1)) {
      $output = theme('pager_last', array('text' => $text, 'element' => $element, 'parameters' => $parameters));
    }
    // The next page is not the last page.
    else {
      $output = theme('pager_link', array('text' => $text, 'page_new' => $page_new, 'element' => $element, 'parameters' => $parameters));
    }
  } else {
    $output = '<span>&nbsp;</span>';
  }

  return $output;
}


/**
 * Prevent Specific module stylesheets. - AJC
 */
function custom_css_alter(&$css) {
  // get rid of problem css
  unset($css['misc/ui/jquery.ui.core.css']);
  unset($css['misc/ui/jquery.ui.theme.css']);
  unset($css['misc/ui/jquery.ui.accordion.css']);


  unset($css['sites/all/libraries/chosen/chosen.css']);
  unset($css[drupal_get_path('module', 'chosen') . '/css/chosen-drupal.css']);
  unset($css[drupal_get_path('module', 'chosen') . '/css/chosen-drupal-rtl.css']);

  unset($css[drupal_get_path('module', 'system') . '/system.menus.css']);
  unset($css[drupal_get_path('module', 'system') . '/system.menus-rtl.css']);
  unset($css[drupal_get_path('module', 'system') . '/system.theme.css']);
  unset($css[drupal_get_path('module', 'system') . '/system.theme-rtl.css']);
  unset($css[drupal_get_path('module', 'system') . '/system.messages.css']);
  unset($css[drupal_get_path('module', 'system') . '/system.messages-rtl.css']);

  unset($css[drupal_get_path('module', 'user') . '/user.css']);
  unset($css[drupal_get_path('module', 'user') . '/user-rtl.css']);

  unset($css[drupal_get_path('module', 'search') . '/search.css']);
  unset($css[drupal_get_path('module', 'search') . '/search-rtl.css']);

  $views_path = drupal_get_path('module', 'views').'/css';
  unset($css[$views_path . '/views.css']);
  unset($css[$views_path . '/views-rtl.css']);

  $addressfield_path = drupal_get_path('module', 'addressfield');
  unset($css[$addressfield_path . '/addressfield.css']);
  unset($css[$addressfield_path . '/addressfield-rtl.css']);

  $field = drupal_get_path('module', 'field');
  unset($css[$field . '/theme/field.css']);
  unset($css[$field . '/theme/field-rtl.css']);

  $references_dialog = drupal_get_path('module', 'references_dialog');
  unset($css[$references_dialog . '/css/references-dialog-admin.css']);
  unset($css[$references_dialog . '/css/references-dialog-admin-rtl.css']);
  unset($css[$references_dialog . '/css/references-dialog-search.css']);
  unset($css[$references_dialog . '/css/references-dialog-search-rtl.css']);

  $date_path = drupal_get_path('module', 'date');
  unset($css[$date_path . '/date_popup/themes/datepicker.1.7.css']);
}


/**
 * Process function for flexslider
 */
function custom_process_flexslider(&$vars) {
  // Add optionset name to the class attribute
  $optionset_name = $vars['settings']['optionset']->name;
  $vars['settings']['attributes']['class'][] = $optionset_name;
}



/**
 * Implements hook_preprocess_field().
 */
function custom_preprocess_field(&$vars, $hook) {
  $element = $vars['element'];

  $vars['items'] = array();
  foreach ($element['#items'] as $delta => $item) {
    if (!empty($element[$delta])) {
      $vars['items'][$delta] = $element[$delta];
    }
  }
  // We can use this to add vars or classes and such to specific fields
  switch ($element['#field_name']) {
    case 'field_post-date':
    case 'field_author':
    case 'field_position':
      $vars['classes_array'][] = 'meta';
    break;
    case 'field_topics':
      $vars['classes_array'][] = 'meta--term';
    break;
    case 'field_image':
      if ($element['#view_mode'] === 'tile') {
        $vars['bgs'] = array();
        foreach ($element['#items'] as $delta => $item) {
          if (empty($elment[$delta]['#image_style'])) {
            $vars['bgs'][$delta] = file_create_url($item['uri']);
          } else {
            $vars['bgs'][$delta] = image_style_url($elment[$delta]['#image_style'], file_uri_target($item['uri']));
          }
        }
      }
    break;
  }
}


function custom_file_widget_multiple($vars) {
  $element = $vars['element'];

  // Special ID and classes for draggable tables.
  $weight_class = $element['#id'] . '-weight';
  $table_id = $element['#id'] . '-table';

  // Build up a table of applicable fields.
  $headers = array();
  $headers[] = t('File information');
  if ($element['#display_field']) {
    $headers[] = array(
      'data' => t('Display'),
      'class' => array('checkbox'),
    );
  }
  $headers[] = t('Weight');
  $headers[] = t('Operations');

  // Get our list of widgets in order (needed when the form comes back after
  // preview or failed validation).
  $widgets = array();
  foreach (element_children($element) as $key) {
    $widgets[] = &$element[$key];
  }
  usort($widgets, '_field_sort_items_value_helper');

  $rows = array();
  foreach ($widgets as $key => &$widget) {
    // Save the uploading row for last.
    if ($widget['#file'] == FALSE) {
      $widget['#title'] = isset($element['#file_upload_title']) ? $element['#file_upload_title'] : NULL;
      $widget['#description'] = isset($element['#file_upload_description']) ? $element['#file_upload_description'] : NULL;
      continue;
    }

    // Delay rendering of the buttons, so that they can be rendered later in the
    // "operations" column.
    $operations_elements = array();
    foreach (element_children($widget) as $sub_key) {
      if (isset($widget[$sub_key]['#type']) && $widget[$sub_key]['#type'] == 'submit') {
        hide($widget[$sub_key]);
        $operations_elements[] = &$widget[$sub_key];
      }
    }

    // Delay rendering of the "Display" option and the weight selector, so that
    // each can be rendered later in its own column.
    if ($element['#display_field']) {
      hide($widget['display']);
    }
    hide($widget['_weight']);

    // Render everything else together in a column, without the normal wrappers.
    $widget['#theme_wrappers'] = array();
    $information = drupal_render($widget);

    // Render the previously hidden elements, using render() instead of
    // drupal_render(), to undo the earlier hide().
    $operations = '';
    foreach ($operations_elements as $operation_element) {
      $operations .= render($operation_element);
    }
    $display = '';
    if ($element['#display_field']) {
      unset($widget['display']['#title']);
      $display = array(
        'data' => render($widget['display']),
        'class' => array('checkbox'),
      );
    }
    $widget['_weight']['#attributes']['class'] = array($weight_class);
    $weight = render($widget['_weight']);

    // Arrange the row with all of the rendered columns.
    $row = array();
    $row[] = $information;
    if ($element['#display_field']) {
      $row[] = $display;
    }
    $row[] = $weight;
    $row[] = $operations;
    $rows[] = array(
      'data' => $row,
      'class' => isset($widget['#attributes']['class']) ? array_merge($widget['#attributes']['class'], array('draggable')) : array('draggable'),
    );
  }

  drupal_add_tabledrag($table_id, 'order', 'sibling', $weight_class);

  $output = '';
  $output = empty($rows) ? '' : theme('table', array('header' => $headers, 'rows' => $rows, 'attributes' => array('id' => $table_id)));
  $output .= drupal_render_children($element);
  return $output;
}

// This is to fix the conflict between metatag and RDF until
// https://www.drupal.org/node/2001836 is resolved, which requires
// progress on https://www.drupal.org/node/1323830.
function custom_html_head_alter(&$head_elements) {
    if (isset($head_elements['rdf_node_title'])){
        unset($head_elements['rdf_node_title']);
    }
    if(isset($head_elements['rdf_node_comment_count'])){
        unset($head_elements['rdf_node_comment_count']);
    }
}

function custom_select($vars) {
  $element = $vars['element'];
  element_set_attributes($element, array('id', 'name', 'size'));
  $select_classes = array('form-select', 'ui', 'selection', 'dropdown');
  if ($element['#multiple']) $select_classes[] = 'search';
  _form_set_class($element, $select_classes);

  return '<select' . drupal_attributes($element['#attributes']) . '>' . form_select_options($element) . '</select>';
}

function custom_file($vars) {
  $element = $vars['element'];
  $element['#attributes']['type'] = 'file';
  element_set_attributes($element, array('id', 'name', 'size'));
  _form_set_class($element, array('form-file'));

  $label = '<label class="file-label needsclick" for="'. $element['#attributes']['id'] . '">' . t('Choose file') . '</label>';
  $labelMate = '<span class="label-mate">' . t('No file selected') .'</span>';

  return '<div class="file-wrapper"><input' . drupal_attributes($element['#attributes']) . ' />' . $label . $labelMate . '</div>';
}
